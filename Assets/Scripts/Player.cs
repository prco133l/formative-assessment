using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    Rigidbody _rigidbody;
    Vector3 position;
    Vector3 mouseDir;
    public float rightScreen;
    public float leftScreen;
    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {   
        
        float inputX = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, 0, 50)).x;

        position = new Vector3(inputX, -17, 0);

        //Vector3  mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //mousePos.z = 0;
        //mousePos.y = -17;
        //mouseDir = mousePos - gameObject.transform.position;
        //mouseDir = mouseDir.normalized;
    }

    void FixedUpdate()
    {   
        if(position.x < leftScreen)
        {
            _rigidbody.MovePosition(new Vector3(leftScreen, position.y, position.z));
        }
        else if(position.x > rightScreen)
        {
            _rigidbody.MovePosition(new Vector3(rightScreen, position.y, position.z));
        }
        else
        {
            _rigidbody.MovePosition(position);
        }

        //_rigidbody.AddForce((mouseDir) * 1f, ForceMode.VelocityChange);

        //_rigidbody.velocity = (mouseDir * 100f);
    }
}